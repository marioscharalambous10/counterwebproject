package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class BaseControllerTest {

	@Test
	public void testWelcome() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcome(map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - "));
		verify(map).addAttribute(eq("counter"), anyInt());
	}

	@Test
	public void testWelcomeName() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcomeName("test", map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - test -"));
		verify(map).addAttribute(eq("counter"), anyInt());
	}

	@Test
	public void testAboutPage() throws Exception {
		ModelMap map = mock(ModelMap.class);
		String result = new About().desc();
		assertTrue("Assert About Page", result.contains("application was copied"));
	}

	@Test
	public void testCalculatorPage() throws Exception {
		ModelMap map = mock(ModelMap.class);
		int result = new Calcmul().mul();
		assertEquals("Assert mul Page", 18, result);
	}

	@Test
	public void testCalcmulPage() throws Exception {
		ModelMap map = mock(ModelMap.class);
		int result = new Calculator().add();
		assertEquals("Assert Calculator add Page", 9, result);
	}

	@Test
	public void testCntPage() throws Exception {
		ModelMap map = mock(ModelMap.class);
		int result = new Counter().divide(4,2);
		assertEquals("Assert Cnt Page", 2, result);
	}

	@Test
	public void testCntPageCondition() throws Exception {
		ModelMap map = mock(ModelMap.class);
		int result = new Counter().divide(4,0);
		assertEquals("Assert Cnt Page Conditional", Integer.MAX_VALUE, result);
	}
}
